from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Company
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.core import serializers
import json

# Create your views here.
response={}
def index(request, id):
    response['login'] = True
    html = 'b2_profil/b2_profil.html'
    company = Company.objects.get(id__exact=id)
    response['name'] = company.name
    response['email'] = company.email
    response['com_type'] = company.com_type
    response['industri'] = company.industri
    response['website'] = company.website
    response['logo_url'] = company.logo_url
    response['size'] = company.size
    response['desc'] = company.desc
    response['founded_year'] = company.founded_year
    response['followers'] = company.followers
    response['city'] = company.city
    print(company.city)
    return render(request, html, response)

@csrf_exempt
def add_company(request):
    id = request.POST.get('id')
    print(type(id))
    print('id '+id)
    if (Company.objects.filter(id__exact=id).count()> 0):
        print("masuk if")
        return HttpResponse(id)
    name = request.POST.get('name')
    email = request.POST.get('email')
    print(email)
    com_type = request.POST.get('com_type')
    industri = request.POST.get('industri')
    website = request.POST.get('website')
    logo_url = request.POST.get('logo_url')
    print(logo_url)
    size = request.POST.get('size')
    desc = request.POST.get('desc')
    founded_year = request.POST.get('founded_year')
    followers = request.POST.get('followers')
    city = request.POST.get('city')
    company = Company(id=id, name=name,email=email,com_type=com_type,industri=industri,website=website,logo_url=logo_url,size=size,desc=desc,founded_year=founded_year,followers=followers,city=city)
    company.save()
    return HttpResponse(id)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data


def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

    
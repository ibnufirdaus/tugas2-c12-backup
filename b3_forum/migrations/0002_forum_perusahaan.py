# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 22:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('b2_profil', '0005_auto_20171212_2331'),
        ('b3_forum', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='forum',
            name='perusahaan',
            field=models.ForeignKey(default=django.utils.timezone.now, on_delete=django.db.models.deletion.CASCADE, to='b2_profil.Company'),
            preserve_default=False,
        ),
    ]

from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from b3_forum.models import Forum
from .models import Reply
from .forms import ReplyForm
# Create your views here.

def add_reply(request,id):
    response = {'replyform': ReplyForm, 'id':id}
    html = 'tanggapan_form_page.html'
    return render(request,html,response)

def submit_reply(request, id):
    forum = Forum.objects.get(id=id)
    reply = ReplyForm(request.POST)
    if(reply.is_valid()):
        sender = reply.cleaned_data["sender"]
        message = reply.cleaned_data["message"]
        print(sender,message)
        Reply.objects.create(sender=sender,message=message, forum=forum)
        if(reply.is_valid()):
            return redirect('/login/')
        else:
            response = {'id': id}
            html = 'tanggapan_form_page.html'
            return render(request,html,response)
    else:
        add_reply(request,id)

def paginate(request, forum_list):
    # Paginator
    paginator = Paginator(forum_list, 3)

    try:
        forum = paginator.page(request)
    except PageNotAnInteger:
        forum = paginator.page(1)
    except EmptyPage:
        forum = paginator.page(paginator.num_pages)

    index = forum.number - 1

    max_index = len(paginator.page_range)

    start_index = index if index >= 10 else 0
    end_index = 10 if index < max_index - 10 else max_index

    page_range = list(paginator.page_range)[start_index:end_index]

    data_paginate = {'forum': forum, 'page_range': page_range}
    return data_paginate